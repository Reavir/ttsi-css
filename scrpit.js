var slider = document.getElementById("speed_range");
var output = document.getElementById("speed_value");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}